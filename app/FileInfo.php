<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileInfo extends Model {

    protected $fillable = [
        'filename',
        'file_header',
        'data',
    ];

}
