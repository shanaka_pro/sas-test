<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {

    protected $table = 'students';
    protected $fillable = [
        'class_room_id',
        'teacher_id',
        'firstname',
        'lastname',
        'gender_id',
        'joined_year',
    ];

    public function gender() {
        return $this->hasOne(Gender::class, 'id', 'gender_id');
    }

    public function classRoom() {
        return $this->hasOne(ClassRoom::class, 'id', 'class_room_id');
    }

    public function teacher() {
        return $this->hasOne(Teacher::class, 'id', 'teacher_id');
    }

    /**
     * 
     * get location language wise
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function storeData($fileData, $groupId) {

        try {

            $csv_data = json_decode($fileData->data, true);

            unset($csv_data[0]);

            $result = $data = array();
            foreach ($csv_data as $rows) {

                $class_room = $rows[0];
                $teacher_name = $rows[1];
                $firstname = $rows[2];
                $lastname = $rows[3];
                $gender = $rows[4];
                $joined_year = $rows[5];

                $class_room_model = ClassRoom::where('name', $class_room)->first(['id']);
                $class_room_id = isset($class_room_model) ? $class_room_model->id : 0;

                $teacher_model = Teacher::where('name', $teacher_name)->first(['id']);
                $teacher_id = isset($teacher_model) ? $teacher_model->id : 0;

                $gender_model = Gender::where('code', $gender)->first(['id']);
                $gender_id = isset($gender_model) ? $gender_model->id : 0;

                $data['class_room_id'] = $class_room_id;
                $data['teacher_id'] = $teacher_id;
                $data['firstname'] = $firstname;
                $data['lastname'] = $lastname;
                $data['gender_id'] = $gender_id;
                $data['joined_year'] = $joined_year;
                $data['created_at'] = now();
                $data['updated_at'] = now();

                $result[] = $data;
            }
            Student::insert($result);
            return true;
        } catch (\Exception $ex) {

            return redirect()->back()->withErrors([$ex->getMessage()]);
        }

        return false;
    }

}
