<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CsvImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\FileInfo;
use App\ClassRoom;
use App\Teacher;
use App\Gender;
use App\Student;

class StudentController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $students = Student::latest('id', 'asc')->paginate(5);

        return view('students.index', compact('students'))
                        ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $classrooms = ClassRoom::pluck('name', 'id');
        $teachers = Teacher::pluck('name', 'id');
        $genders = Gender::pluck('name', 'id');

        $years = [];
        $now_year = date('Y');
        for ($year = 2000; $year <= $now_year; $year++)
            $years[$year] = $year;

        return view('students.create', compact('classrooms', 'teachers', 'genders', 'years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'class_room_id' => 'required|integer',
            'teacher_id' => 'required|integer',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'gender_id' => 'required|integer',
            'joined_year' => 'required',
        ]);

        Student::create($request->all());

        return redirect()->route('students.index')
                        ->with(['success' => __('school/common.record_added_successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student) {
        $student->delete();

        return redirect()->route('students.index')
                        ->with(['success' => __('school/common.record_deleted_successfully')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student) {

        $classrooms = ClassRoom::pluck('name', 'id');
        $teachers = Teacher::pluck('name', 'id');
        $genders = Gender::pluck('name', 'id');

        $years = [];
        $now_year = date('Y');
        for ($year = 2000; $year <= $now_year; $year++)
            $years[$year] = $year;

        return view('students.edit', compact('classrooms', 'teachers', 'genders', 'student', 'years'));
    }

    /**
     * Edit student records
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student) {

        request()->validate([
            'class_room_id' => 'required|integer',
            'teacher_id' => 'required|integer',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'gender_id' => 'required|integer',
            'joined_year' => 'required',
        ]);


        $student->update($request->all());
        return redirect()->route('students.index')
                        ->with(['success' => __('school/common.record_updated_successfully')]);
    }

    /**
     * Display the upload view.
     *
     * @return \Illuminate\Http\Response
     */
    public function fileUpload() {

        return view('students.upload');
    }

    /**
     * 
     * import CSV file data
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(CsvImportRequest $request) {

        try {
            $path = $request->file('csv_file')->getRealPath();

            if ($request->has('header')) {
                $data = Excel::load($path, function($reader) {
                            
                        })->get()->toArray();
            } else {
                $data = array_map('str_getcsv', file($path));
            }

            if (count($data) > 0) {

                $csvDataFile = FileInfo::create([
                            'filename' => $request->file('csv_file')->getClientOriginalName(),
                            'file_header' => $request->has('header'),
                            'data' => json_encode($data)
                ]);


                $storeData = Student::storeData($csvDataFile, $request->id);

                if ($storeData) {
                    return redirect()->action('StudentController@index')->with(['success' => __('school/common.record_added_successfully')]);
                } else {

                    return redirect()->back()->withErrors(__('school/common.record_failed_to_add'));
                }
            } else {
                return redirect()->back();
            }
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors([$ex->getMessage()]);
        }
    }

}
