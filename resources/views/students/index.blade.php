@extends('layouts.app')

@section('content')
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    @if(\Session::has('success'))
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    <div class="panel-heading">{{ __('school/common.list_students') }}</div>
    <div class="panel">

        <div class="panel-body bg-panel">

            <div class="col-sm-12 col-md-12 row">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-light">
                        <th>{{ __('school/common.id') }}</th>
                        <th>{{ __('school/common.classroom') }}</th>
                        <th>{{ __('school/common.teachername') }}</th>
                        <th>{{ __('school/common.firstname') }}</th>
                        <th>{{ __('school/common.lastname') }}</th>
                        <th>{{ __('school/common.gender') }}</th>
                        <th>{{ __('school/common.joinedyear') }}</th>
                        <th>{{ __('school/common.createdat') }}</th>
                        <th colspan="2">{{ __('school/common.action') }}</th>            
                        </thead>
                        <tbody>


                            @forelse($students as $student)


                            <tr class="data-row">
                                <!--<td><a href="{{route('students.show', $student->id )}}">{{ $student->id}}</a></td>-->
                                <td>{{ $student->id }}</td>
                                <td>{{ isset($student->classRoom->name) ? $student->classRoom->name : null }}</td>
                                <td>{{ isset($student->teacher->name) ? $student->teacher->name: null }}</td>
                                <td>{{ $student->firstname }}</td>
                                <td>{{ $student->lastname }}</td>
                                <td>{{ isset($student->gender->name) ? $student->gender->name :null }}</td>
                                <td>{{ $student->joined_year }}</td>
                                <td>{{ \Carbon\Carbon::parse($student->created_at)->setTimezone('GMT+8')->format('Y-m-d H:i:s') }}</td>
                                <td> <a href="{{ url('students/'.$student->id . '/edit') }}" class="btn btn-default">{{ __('school/common.action_edit') }}</a>
                                <td>
                                    <form action="{{action('StudentController@destroy', $student->id)}}" method="post">
                                        {{csrf_field()}}
                                        <input name="_method" type="hidden" value="DELETE"/>
                                        <button class="btn btn-danger" type="submit">{{ __('school/common.action_delete') }}</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">{{ __('school/common.no_records_found') }}</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $students->links() !!}
                </div>
            </div>

            <div class="col-sm-12 col-md-12 row">
                <div class="button-div">
                    <a class="btn btn-primary" href="{{ url('/students/create') }}">{{ __('school/common.button_create') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
