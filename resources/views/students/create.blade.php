@extends('layouts.app')

@section('content')
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <div class="panel-heading">{{ __('school/common.add_students') }}</div>
    <div class="panel">
        <div class="panel-body bg-panel">
            <div class="row">
                {!! Form::open(['route' => 'students.store']) !!}
                <div class="col-xs-12 col-sm-12 col-md-6">

                    <div class="form-group">
                        {!! Form::Label('class_room_id', 'Class room:') !!}
                        {!! Form::select('class_room_id',$classrooms, ['class' => 'form-control']) !!}

                    </div>

                    <div class="form-group">
                        {!! Form::Label('teacher_id', 'Teacher`s name:') !!}
                        {!! Form::select('teacher_id',$teachers, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::Label('firstname', 'Student`s first name:') !!}
                        {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::Label('lastname', 'Student`s last name:') !!}
                        {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::Label('gender_id', 'Gender:') !!}
                        {!! Form::select('gender_id',$genders, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::Label('joined_year', 'Joined year:') !!}
                        {!! Form::select('joined_year', $years,['class' => 'form-control']) !!}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>

                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
