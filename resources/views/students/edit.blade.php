@extends('layouts.app')

@section('content')
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <div class="panel-heading">{{ __('school/common.update_students') }}</div>
    <div class="panel">
        <div class="panel-body bg-panel">
            <div class="row">
                {{ Form::model( $student, ['route' => ['students.update', $student->id], 'method' => 'put', 'role' => 'form'] ) }}

                <div class="col-xs-12 col-sm-12 col-md-6">

                    <div class="form-group">
                        {!! Form::Label('class_room_id', 'Class room:') !!}
                        {!! Form::select('class_room_id',$classrooms, ['class' => 'form-control']) !!}

                    </div>
                    <div class="form-group">
                        {!! Form::Label('teacher_id', 'Teacher`s name:') !!}
                        {!! Form::select('teacher_id',$teachers,$student->teacher_id, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::Label('firstname', 'Student`s first name:') !!}
                        {!! Form::text('firstname', $student->firstname, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::Label('lastname', 'Student`s last name:') !!}
                        {!! Form::text('lastname', $student->lastname, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::Label('gender_id', 'Gender:') !!}
                        {!! Form::select('gender_id',$genders,$student->gender_id, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">

                        {!! Form::Label('joined_year', 'Joined year:') !!}
                        {!! Form::select('joined_year',$years,$student->joined_year, ['class' => 'form-control']) !!}

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>

                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
