<?php

return [
    'add_students' => 'Add students',
    'update_students' => 'Update students',
    'list_students' => 'Students list',
    'classroom' => 'Class room',
    'file_import' => 'CSV file import',
    'no_records_found' => 'No records found.',
    'record_added_successfully' => 'Record added successfully.',
    'record_updated_successfully' => 'Record updated successfully.',
    'record_failed_to_add' => 'Failed to add record.',
    'record_deleted_successfully' => 'Record deleted successfully.',
    'inactive' => 'Inactive',
    'active' => 'Active',
    'actions' => 'Actions',
    'action_edit' => 'Edit',
    'action_delete' => 'Delete',
    'button_create' => 'Create',
    'button_edit' => 'Edit',
    'button_upload' => 'Upload',

    'add_item' => 'Add Item',

    'id' => 'ID',
    'classroom' => 'Class room',
    'teachername' => 'Teacher`s name',
    'firstname' => 'First name',
    'lastname' => 'Last name',
    'joinedyear' => 'Joined year',
    'gender' => 'Gender',
    'createdat' => 'Created at',
    'action' => 'Action',
];
