<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use Illuminate\Database\Seeder;
use App\Teacher;
use Carbon\Carbon;

class TeachersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Teacher::insert(
                [
                    'id' => 1,
                    'name' => 'Emily',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
        ]);
        Teacher::insert(
                [
                    'id' => 2,
                    'name' => 'Isabella',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
        ]);
    }

}
