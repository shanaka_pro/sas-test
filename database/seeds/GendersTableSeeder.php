<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use Illuminate\Database\Seeder;
use App\Gender;
use Carbon\Carbon;

class GendersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {


        Gender::insert(
                [
                    'id' => 1,
                    'name' => 'Male',
                    'code' => 'M',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
        ]);
        Gender::insert(
                [
                    'id' => 2,
                    'name' => 'Female',
                    'code' => 'F',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
        ]);
    }

}
