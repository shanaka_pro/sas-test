<?php

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use Illuminate\Database\Seeder;
use App\ClassRoom;
use Carbon\Carbon;

class ClassRoomsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        ClassRoom::insert(
                [
                    'id' => 1,
                    'name' => 'A',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
        ]);
        ClassRoom::insert(
                [
                    'id' => 2,
                    'name' => 'B',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
        ]);
        ClassRoom::insert(
                [
                    'id' => 3,
                    'name' => 'C',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
        ]);
    }

}
